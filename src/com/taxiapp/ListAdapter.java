package com.taxiapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListAdapter extends BaseAdapter {
	Context context;
	String[] textnames;
	int[] images;
	LayoutInflater inflater;

	ListAdapter(Context context, String[] textnames, int[] images) {

		this.context = context;
		this.textnames = textnames;
		this.images = images;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return textnames.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    convertView=inflater.inflate(R.layout.single_list,null);
	    ImageView iv=(ImageView)convertView.findViewById(R.id.imageView1);
	    iv.setImageResource(images[position]);
	    TextView tv=(TextView)convertView.findViewById(R.id.textView1);
	    tv.setText(textnames[position]);
		return convertView;

	}

	
}

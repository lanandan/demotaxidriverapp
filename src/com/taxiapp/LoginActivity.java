package com.taxiapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity {
	Button login, register;
	TextView forgotpwd;
	Util checkconnectivity;
	boolean isGPSEnabled;
	LocationManager locationManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		login = (Button) findViewById(R.id.button1);
		register = (Button) findViewById(R.id.button2);
		forgotpwd = (TextView) findViewById(R.id.textView1);
		checkconnectivity = new Util(LoginActivity.this);
		locationManager = (LocationManager) LoginActivity.this
				.getSystemService(LOCATION_SERVICE);
		if (checkconnectivity.isConnectingToInternet()) {

			login.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					isGPSEnabled = locationManager
							.isProviderEnabled(LocationManager.GPS_PROVIDER);
					if (isGPSEnabled) {
						Intent i = new Intent(LoginActivity.this,
								HomeScreen.class);
						startActivity(i);
					} else {
						ShowGpsUserSettings();

					}
				}
			});

		} else {
			Toast.makeText(getApplicationContext(),
					"check Internet connectivity", Toast.LENGTH_SHORT).show();
		}
		if (checkconnectivity.isConnectingToInternet()) {
			register.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent i = new Intent(LoginActivity.this,
							RegistrationActivity.class);
					startActivity(i);
				}
			});
		} else {
			Toast.makeText(getApplicationContext(),
					"check Internet connectivity", Toast.LENGTH_SHORT).show();
		}
		if (checkconnectivity.isConnectingToInternet()) {

			forgotpwd.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent i = new Intent(LoginActivity.this, ForgotPwd.class);
					startActivity(i);
				}
			});
		} else {
			Toast.makeText(getApplicationContext(),
					"check Internet connectivity", Toast.LENGTH_SHORT).show();
		}
	}

	public void ShowGpsUserSettings() {
		// TODO Auto-generated method stub
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				LoginActivity.this);
		alertDialog.setTitle("GPS is settings");
		alertDialog
				.setMessage("GPS is wnot enabled. Do you want to go to settings menu?");
		alertDialog.setPositiveButton("Settings",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						LoginActivity.this.startActivityForResult(intent, 100);

					}
				});
		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		alertDialog.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		Log.i("test", "onActivity Result is trigged");
		if (requestCode == 100 && resultCode == 0) {
			isGPSEnabled = locationManager
					.isProviderEnabled(LocationManager.GPS_PROVIDER);
			if (isGPSEnabled) {
				Intent i = new Intent(LoginActivity.this, HomeScreen.class);

				startActivity(i);
			} else {
				ShowGpsUserSettings();

			}
		}
	}
}

package com.taxiapp;
import android.app.Activity;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {
	Util checkconnectivity;
	LocationManager locationManager;
	Handler handle = new Handler();
	boolean isGPSEnabled;
	TextView tv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		tv = (TextView) findViewById(R.id.textView1);
		locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
		isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		checkconnectivity = new Util(MainActivity.this);
		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		if (checkconnectivity.isConnectingToInternet()) {

			handle.postDelayed(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					Intent i = new Intent(MainActivity.this,LoginActivity.class);
					startActivity(i);
					finish();
				}
			}, 3000);

		}

		else {

			tv.setVisibility(View.VISIBLE);
		}

	}

}

package com.taxiapp;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

@SuppressLint("NewApi")
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class NavigationDrawerFragment extends Fragment {
	ActionBarDrawerToggle mDrawerToggle;
	DrawerLayout mDrawerLayout;
	boolean nottolandscape;
	boolean savedinstancestate;
	String key = "prefs";
	View containerview;
	ListView listview;
    TextView person, mobileno;
	ListAdapter adapter;
	public static int FragIndex;
	Fragtoactivity frag;
	ListAdapter listAdapter;
	int index;
	String[] textnames = { "Home", "Profile", "Logout" };
	int[] images = { R.drawable.home, R.drawable.people, R.drawable.logout };
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(inflater, container, savedInstanceState);
		Log.i("test", "cmg to on create view");
		View v = inflater.inflate(R.layout.nav_drawer, container, false);
		person = (TextView) v.findViewById(R.id.textView1);
		mobileno = (TextView) v.findViewById(R.id.textView2);
		listview = (ListView) v.findViewById(R.id.listview);
		listAdapter = new ListAdapter(getActivity(), textnames, images);
		listview.setAdapter(listAdapter);
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				index = position;
				frag.datapass(index);
				mDrawerLayout.closeDrawer(containerview);
			}
		});

		return v;
	}

	public void setUp(int navdrawer, DrawerLayout drawerlayout, Toolbar toolbar) {
		// TODO Auto-generated method stub
		containerview = getActivity().findViewById(navdrawer);
		mDrawerLayout = drawerlayout;
		mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerlayout,
				toolbar, R.string.Open, R.string.Close) {

			@Override
			public void onDrawerOpened(View drawerView) {
				// TODO Auto-generated method stub
				super.onDrawerOpened(drawerView);
				getActivity().invalidateOptionsMenu();
			}

			@Override
			public void onDrawerClosed(View drawerView) {

				// TODO Auto-generated method stub

				super.onDrawerClosed(drawerView);
				getActivity().invalidateOptionsMenu();
			}

		};

		if (nottolandscape && savedinstancestate == false) {
			mDrawerLayout.openDrawer(containerview);
		}
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		mDrawerLayout.post(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				mDrawerToggle.syncState();
			}
		});
	}

	 @Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		frag = (Fragtoactivity) activity;
	}
}
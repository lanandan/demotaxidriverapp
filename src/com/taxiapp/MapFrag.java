package com.taxiapp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MapFrag extends Fragment {
	public static View v;
	double lat, lat1, lng, lng1, lat2, lng2;
	GPSTracker gpstracker;
	GoogleMap googlemap;
	Button b1, b2;
	int temp;
	Marker marked1, marked2;
	double latitudes[] = { 13.067906, 13.065522, 13.063222, 13.060118,
			13.057968, 13.057834, 13.051782, 13.039504, 13.101423, 13.0405026 };
	double longitudes[] = { 80.226852, 80.242198, 80.236282, 80.238622,
			80.236264, 80.236105, 80.234036, 80.232346, 80.289226, 80.289226 };
	Util checkconnectivity;

	public static String url;
	ArrayList<LatLng> markerPoints;

	public void ProcessMap() {
		// TODO Auto-generated method stub
		if (googlemap == null) {
			try {
				googlemap = ((MapFragment) getFragmentManager()
						.findFragmentById(R.id.map)).getMap();
			} catch (Exception e) {
				Log.i("test", "the exception is " + e);
			}
		}
		if (googlemap != null) {
			gpstracker = new GPSTracker(getActivity().getApplicationContext());
			lat = gpstracker.getLatitude();
			lng = gpstracker.getLongitude();
			Log.i("test", "the latitude is " + lat);
			Log.i("test", "the longitude is " + lng);
			lat1 = 13.040502600000000000;
			lng1 = 80.233692400000000000;
			MarkerOptions marker = new MarkerOptions().position(
					new LatLng(lat, lng)).title("Taxi1");
			marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.taxi2));
			marked1 = googlemap.addMarker(marker);
			MarkerOptions marker1 = new MarkerOptions().position(
					new LatLng(lat1, lng1)).title("person");
			marker1.icon(BitmapDescriptorFactory
					.fromResource(R.drawable.person1));
			CameraPosition cameraPosition1 = new CameraPosition.Builder()
					.target(new LatLng(lat1, lng1)).zoom(13).bearing(90)
					.tilt(40).build();
			googlemap.animateCamera(CameraUpdateFactory
					.newCameraPosition(cameraPosition1));
			marked2 = googlemap.addMarker(marker1);

		}
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (v != null) {
			ViewGroup parent = (ViewGroup) v.getParent();
			if (parent != null)
				parent.removeView(v);
		}
		try {
			v = inflater.inflate(R.layout.mapfrag, container, false);
			b1 = (Button) v.findViewById(R.id.button1);
			b2 = (Button) v.findViewById(R.id.button2);
			markerPoints = new ArrayList<LatLng>();
			ProcessMap();
			b1.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					b1.setText("Start Driving");
					b2.setVisibility(View.GONE);
					url = getDirectionsUrl(lat, lng, lat1, lng1);
					Log.i("test", "the url is" + url);
					new DownloadTask().execute();

					b1.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							/*Handler handler = new Handler();
							Log.i("test", "the length of lat"
									+ latitudes.length);
							Log.i("test", "the length of lng"
									+ longitudes.length);
							for (int i = 0; i < latitudes.length; i++) {
								temp = i;
								Log.i("test", "the values of" + temp);
								Log.i("test", "the values of" + latitudes[temp]);
								lat2 = latitudes[temp];
								Log.i("test", "the values of" + lat);
								lng2 = longitudes[temp];
								MarkerOptions marker2 = new MarkerOptions()
										.position(new LatLng(lat2, lng2))
										.title("person");
								marker2.icon(BitmapDescriptorFactory
										.fromResource(R.drawable.taxi2));
								CameraPosition cameraPosition2 = new CameraPosition.Builder()
										.target(new LatLng(lat2, lng2))
										.zoom(13).bearing(90).tilt(40).build();
								googlemap.animateCamera(CameraUpdateFactory
										.newCameraPosition(cameraPosition2));
								googlemap.addMarker(marker2);
								marked2.remove();
								try {
									Thread.sleep(3000);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}
*/							b1.setText("Reached");
							b1.setOnClickListener(new OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									b1.setText("Completed");

								}
							});
						}
					});
				}
			});
			b2.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					b1.setVisibility(View.GONE);
					b2.setVisibility(View.GONE);
				}
			});
		} catch (Exception e) {
			Log.i("test", "the exception is " + e);

		}
		return v;
	}

	private String getDirectionsUrl(double lat, double lng, double lat1,
			double lng1) {
		String str_origin = "origin=" + lat + "," + lng;
		String str_dest = "destination=" + lat1 + "," + lng1;
		String sensor = "sensor=false";
		String parameters = str_origin + "&" + str_dest + "&" + sensor;
		String output = "json";
		String url = "https://maps.googleapis.com/maps/api/directions/"
				+ output + "?" + parameters;
		return url;
	}

	class DownloadTask extends AsyncTask<Void, Void, String> {
		DownloadUrl download = new DownloadUrl();

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			String data = "";
			checkconnectivity = new Util(getActivity());
			try {
				if (checkconnectivity.isConnectingToInternet()) {
					data = download.downloadUrls(url);
				} else {
					Toast.makeText(getActivity(),
							"check Internet Connectivity", Toast.LENGTH_SHORT)
							.show();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.e("test", "the exception in downloadtask" + e);
			}

			return data;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub

			new ParserTask().execute(result);
		}

	}

	class ParserTask extends
			AsyncTask<String, Void, List<List<HashMap<String, String>>>> {

		@Override
		protected List<List<HashMap<String, String>>> doInBackground(
				String... params) {

			JSONObject jObject;
			List<List<HashMap<String, String>>> routes = null;

			try {
				if (checkconnectivity.isConnectingToInternet()) {
					jObject = new JSONObject(params[0]);
					DirectionsJSONParser parser = new DirectionsJSONParser();
					routes = parser.parse(jObject);
				} else {
					Toast.makeText(getActivity(),
							"check Internet Connectivity", Toast.LENGTH_SHORT)
							.show();

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return routes;
		}

		@Override
		protected void onPostExecute(List<List<HashMap<String, String>>> result) {
			// TODO Auto-generated method stub
			ArrayList<LatLng> points = null;
			PolylineOptions lineOptions = null;
			MarkerOptions markerOptions = new MarkerOptions();
			for (int i = 0; i < result.size(); i++) {
				points = new ArrayList<LatLng>();
				lineOptions = new PolylineOptions();

				List<HashMap<String, String>> path = result.get(i);
				for (int j = 0; j < path.size(); j++) {
					HashMap<String, String> point = path.get(j);
					double lat = Double.parseDouble(point.get("lat"));
					double lng = Double.parseDouble(point.get("lng"));
					LatLng position = new LatLng(lat, lng);
					points.add(position);
				}
				lineOptions.addAll(points);
				lineOptions.width(5);
				lineOptions.color(Color.BLACK);

			}
			googlemap.addPolyline(lineOptions);
			googlemap.setMyLocationEnabled(true);
			googlemap.getUiSettings().setZoomControlsEnabled(false);
			googlemap.getUiSettings().setMyLocationButtonEnabled(true);
			googlemap.getUiSettings().setCompassEnabled(true);
			googlemap.getUiSettings().setRotateGesturesEnabled(true);
			googlemap.getUiSettings().setZoomGesturesEnabled(true);
		}

	}

}

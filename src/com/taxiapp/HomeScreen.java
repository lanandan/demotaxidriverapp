package com.taxiapp;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import com.google.android.gms.maps.GoogleMap;

@SuppressWarnings("deprecation")
@SuppressLint("NewApi")
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class HomeScreen extends ActionBarActivity implements Fragtoactivity {
	public static Toolbar toolbar;
	GoogleMap googleMap;
	DrawerLayout mdrwrly;
	NavigationDrawerFragment mFragment;
	MapFrag mapfrag=new MapFrag();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nextscreen);
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		mdrwrly = (DrawerLayout) findViewById(R.id.drawer_layout);
		mFragment = (NavigationDrawerFragment) getFragmentManager()
				.findFragmentById(R.id.fragment_navigation_drawer);
		mFragment.setUp(R.id.fragment_navigation_drawer, mdrwrly, toolbar);
		FragmentManager fm = getFragmentManager();
		android.app.FragmentTransaction ft = fm.beginTransaction();
		ft.add(R.id.frame_layout,mapfrag);
		ft.commit();

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mapfrag.ProcessMap();
	}

	@Override
	public void datapass(int index) {
		// TODO Auto-generated method stub
		switch (index) {
		case 0:
			Log.i("test", "calling the map frag");
			FragmentManager fm = getFragmentManager();
			android.app.FragmentTransaction ft = fm.beginTransaction();
			ft.replace(R.id.frame_layout, mapfrag);
			ft.commit();
			break;
		case 1:
			Log.i("test", "calling the Profile Frag");
			FragmentManager fm1 = getFragmentManager();
			android.app.FragmentTransaction ft1 = fm1.beginTransaction();
			ft1.replace(R.id.frame_layout, new ProfileFrag());
			ft1.commit();
			break;
		case 2:
			Intent i = new Intent(HomeScreen.this, LoginActivity.class);
			startActivity(i);
			finish();
			break;
		}

	}

}